#base image
FROM python:3.7

#File Author / Maintainer
MAINTAINER Isaac Mendez
#adding projects file to the path of project
ADD . /home/imendez/Projects/wholeNotes/
#set directory where CMD will execute
WORKDIR /home/imendez/Projects/wholeNotes

#Get pip to download and install requirements:
RUN pip install --no-cache-dir -r requirements.txt

#Expose ports
EXPOSE 8000
#default command to execute
CMD exec gunicorn wholeNotes.wsgi:application --bind 0.0.0.0:8000 --workers 3
