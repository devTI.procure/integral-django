from django.db import models
import uuid
from django.utils import timezone

class Personal(models.Model):
    id = models.CharField(max_length=128, primary_key=True, default=uuid.uuid4)
    name = models.CharField(max_length=32)
    computer_code = models.CharField(max_length=32)
    ubication = models.CharField(max_length=32)
    last_updated = models.DateTimeField(auto_now=True)
    def __str__(self):
        return self.name+ ' ' +self.computer_code

class License(models.Model):
    id = models.CharField(max_length=128, primary_key=True, default=uuid.uuid4)
    software_name = models.CharField(max_length=50)
    key = models.CharField(max_length=32)
    description = models.TextField(blank=True)
    owner = models.ForeignKey(Personal, on_delete=models.DO_NOTHING)

class Note(models.Model):
    id = models.CharField(max_length=128, primary_key=True, default=uuid.uuid4)
    software = models.CharField(max_length=50, blank=True)
    version = models.CharField(max_length=15, blank=True, null=True)
    description = models.TextField(blank=True)
    date_created = models.DateTimeField(default=timezone.now)
    owner = models.ForeignKey(Personal, on_delete=models.DO_NOTHING)
    class Meta:
        ordering = ['-date_created']

    #correlative
    #manager
    #email