from django.forms import ModelForm
from .models import Note, License, Personal
from django.forms import ModelForm, Textarea

class NotesForm(ModelForm):
    class Meta:
        model = Note
        fields = ('software', 'version', 'description', 'date_created', 'owner')
        #fields = '__all__'
        """widgets = {
            'description' : Textarea(attrs={'cols': 20, 'rows': 10}),
        }"""


class LicensesForm(ModelForm):
    class Meta:
        model = License
        fields = ('software_name', 'key' ,'description', 'owner')
        #fields = '__all__'

class PersonalForm(ModelForm):
    class Meta:
        model = Personal
        fields = ('name', 'computer_code', 'ubication')
