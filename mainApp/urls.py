from django.urls import path
from .views import *


urlpatterns = [
    path("", Listing, name="listing"),
    path("add/", Add, name="add"),
    path("edit/<pk>", Editing, name="edit"),
    path("destroy/<pk>", Destroying, name="destroy"),
    path("", login_required(Listing), name="listing-notes"),
    path("notes/add/", login_required(Add), name="add-notes"),
    path("notes/edit/<pk>", login_required(Editing), name="edit-notes"),
    path("notes/destroy/<pk>", login_required(Destroying), name="destroy-notes"),
    path("licenses/", login_required(LicenseList.as_view()) , name="listing-licenses" ),
    path("licenses/add/", login_required(LicenseAdd.as_view()), name="listing-add"),
    path("licenses/edit/<pk>", login_required(LicenseEdit.as_view()), name="listing-update"),
    path("licenses/delete/<pk>", login_required(LicenseDelete.as_view()), name="listing-delete"),
    path("personal/", login_required(PersonalList.as_view()), name="listing-personal"),
    path("personal/add/", login_required(PersonalAdd.as_view()), name="personal-add"),
    path("personal/edit/<pk>", login_required(PersonalEdit.as_view()), name="personal-edit"),
    path("personal/delete/<pk>", login_required(PersonalDelete.as_view()), name="personal-delete"),
]