from django.contrib import admin
from .models import *

class DataNote(admin.ModelAdmin):
    list_display = ['software', 'version', 'description', 'date_created']

class LicenseList(admin.ModelAdmin):
    list_display = ['software_name', 'description']

class PersonalPC(admin.ModelAdmin):
    list_display = ['name', 'computer_code', 'ubication', 'last_updated']

admin.site.register(Note, DataNote)
admin.site.register(License, LicenseList)
admin.site.register(Personal, PersonalPC)
