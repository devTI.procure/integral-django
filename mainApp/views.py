
from django.shortcuts import render, redirect, render_to_response
from django.http import HttpResponse
from .forms import *
import uuid
from .models import *
from django.contrib.auth.decorators import login_required
#Importing generic viewsl
from django.http import HttpResponseRedirect
from django.views.generic.base import TemplateView
from django.views.generic import ListView, DetailView 
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView
from django.urls import reverse_lazy

@login_required()
def Add(request):
    if request.method == "POST":
        form = NotesForm(request.POST)
        if form.is_valid():
            data = form.save(commit=False)
            data.id = uuid.uuid4()
            data.software = request.POST['software']
            data.version = request.POST.get('version')
            data.description = request.POST['description']
            data.date_created = timezone.now()
            data.save()
            return redirect('listing-notes')
    return render(request, 'software/add.html', {'form': NotesForm()})

@login_required()
def Listing(request):
    data = Note.objects.all()
    return render(request, 'software/show.html', {'list': data})
@login_required()
def Editing(request, pk): 
    form = NotesForm(request.POST or None, instance=Note.objects.get(id=pk))
    if request.method == "POST":
        if form.is_valid():
            data = form.save(commit=False)
            data.code = request.POST['code']
            data.software = request.POST['software']
            data.version = request.POST.get('version')
            data.description = request.POST['description']
            data.date_created = timezone.now()
            data.save()
            return redirect('listing-notes')
    return render(request, 'software/add.html', {'form': form})


@login_required()
def Destroying(request, pk): 
    data = Note.objects.get(id=pk)
    aux = data
    data.delete()
    return render(request, 'software/destroy.html', {'data': aux})

class LicenseList(ListView):
    model = License
    template_name = 'licenses/show.html'

class LicenseAdd(FormView):
    model = License
    form_class = LicensesForm
    template_name = 'licenses/add.html'
    success_url = '/licenses/'

    def form_valid(self, form):
        form.id = uuid.uuid4()
        form.save()
        return super().form_valid(form)

class LicenseEdit(UpdateView):
    model = License
    form_class = LicensesForm
    template_name = 'licenses/add.html'
    success_url = reverse_lazy("listing-licenses")


class LicenseDelete(DeleteView):
    model = License
    success_url = reverse_lazy("listing-licenses")
    #def get_success_url(self, *args, **kwargs):
    #    context = self.kwargs['pk']
    #    return  reverse_lazy('listing-delete', kwargs={'pk': context})
    def get(self, *args, **kwargs):
        data = self.get_object()
        info = {'id': data.id, 'software_name': data.software_name, 'key': data.key, 'description': data.description}
        self.delete(*args, **kwargs)
        return render_to_response( 'licenses/destroy.html', {'object': info})

class PersonalList(ListView):
    model = Personal
    template_name = 'personal/show.html'

class PersonalAdd(FormView):
    model = Personal
    form_class = PersonalForm
    template_name = 'personal/add.html'
    success_url = reverse_lazy('listing-personal')

    def form_valid(self, form):
        form.id = uuid.uuid4()
        form.save()
        return super().form_valid(form)

class PersonalEdit(UpdateView):
    model = Personal
    form_class = PersonalForm
    template_name = 'personal/add.html'
    success_url = reverse_lazy('listing-personal')

class PersonalDelete(DeleteView):
    model = Personal
    success_url = reverse_lazy('listing-personal')

    def get(self, *args, **kwargs):
        data = self.get_object()
        info = {'id': data.id, 'name': data.name, 'computer_code': data.computer_code, 'ubication': data.ubication}
        self.delete(*args, **kwargs)
        return render_to_response( 'personal/destroy.html', {'object': info})
"""
from django.http import Http404
    def get_object(self, queryset=None):
        # Hook to ensure object is owned by request.user.
        obj = super(MyDeleteView, self).get_object()
        if not obj.owner == self.request.user:
            raise Http404
        return obj"""
        
